cmake_minimum_required(VERSION 3.17.0)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

project(rc_wolf LANGUAGES C)
add_subdirectory("src")

find_package(SDL2 REQUIRED)
