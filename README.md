Simple raycasting renderer based on lodev.org tutorial
=========================

[lodev.org raycasting](http://lodev.org/cgtutor/raycasting.html)

![Screenshot](https://bitbucket.org/80286/rc_wolf/raw/master/res/140116_rc.png)

How to build & launch
--------------

- `git clone https://bitbucket.org/80286/rc_wolf.git && cd rc_wolf`
- `mkdir build`
- `cd build && cmake ..`
- `make`
- `./rc_wolf`


Keybindings
---------------
- `F1` - enable/disable textures
- `W` `S` `A` `D` / cursors - navigation
