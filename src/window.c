#include "rc.h"
#include "window.h"
#include <SDL2/SDL.h>

#define  WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 720
#define WINDOW_TITLE "RC"

SDL_Window *window;
SDL_Surface *window_data;

void window_init(void) {
    if(SDL_Init(SDL_INIT_VIDEO < 0)) {
        fprintf(stderr, "SDL could not initialize! SDL_Error: %s\n",
                SDL_GetError());
        exit(1);
    }

    window = (SDL_Window *)SDL_CreateWindow(WINDOW_TITLE,
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            WINDOW_WIDTH,
            WINDOW_HEIGHT,
            SDL_WINDOW_SHOWN);

    if(window == NULL) {
        fprintf(stderr, "Unable to initialize window. SDL_Error: %s\n",
                SDL_GetError());
        exit(1);
    }

    window_data = SDL_GetWindowSurface(window);
}

void window_clear(void) {
    const int len = 4 * window_data->w * window_data->h;
    uint8_t *data = window_data->pixels;

    for(int i = 0; i < len; i++) {
        data[i] = 0;
    }
}

void window_update(void) {
    image_t img;

    window_clear();

    img.data = window_data->pixels;
    img.width = window_data->w;
    img.height = window_data->h;

    rc_draw(&img);
    SDL_UpdateWindowSurface(window);
}

void window_close(void) {
    if (window != NULL) {
        SDL_DestroyWindow(window);
        window = NULL;
    }
    SDL_Quit();
}

int main(int argc, char **argv) {
    window_init();

    rc_initTextures();
    rc_main();
    rc_freeTextures();

    window_close();

    return 0;
}
