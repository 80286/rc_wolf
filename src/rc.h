#ifndef RC_H
#define RC_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

struct v2f {
    float x;
    float y;
};

struct v2i {
    int x;
    int y;
};

struct ray_t {
    struct v2i map;
    struct v2f pos;
    struct v2f dir;
    struct v2f dist;
    float realDist;
    int hit;
    int side;
    struct v2i step;
    float dx;
    float tg;
    float ctg;
};

struct image_t {
    uint8_t *data;
    int width;
    int height;
};

struct span_t {
    int x;
    int y1;
    int y2;
};

typedef struct v2f v2f;
typedef struct v2i v2i;
typedef struct image_t image_t;
typedef struct ray_t ray_t;
typedef struct span_t span_t;

void rc_toggleTextures(void);
void rc_rotate(float);
void rc_rotateLeft(void);
void rc_rotateRight(void);
void rc_moveForward(void);
void rc_moveBackward(void);
void rc_strafeLeft(void);
void rc_strafeRight(void);

void rc_initTextures(void);
void rc_freeTextures(void);

void rc_draw(image_t *);
void rc_main(void);

#endif
