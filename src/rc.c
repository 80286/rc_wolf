#include "rc.h"
#include "window.h"
#include <SDL2/SDL.h>

float v2fnorm(v2f *v) {
    return sqrtf(v->x * v->x + v->y * v->y);
}

float v2fdist(v2f *v, v2f *w) {
    v2f d;

    d.x = (v->x - w->x);
    d.y = (v->y - w->y);

    return v2fnorm(&d);
}
void v2frot(v2f *v, float r) {
    float ox, oy;
    float c, s;

    c = cos(r);
    s = sin(r);

    ox = v->x;
    oy = v->y;

    v->x   = c * ox - s * oy;
    v->y   = s * ox + c * oy;
}

#define MAP_DIM 20
#define SIDE_VERTICAL 0
#define SIDE_HORIZONTAL 1

#define TEX_WIDTH 64
#define TEX_HEIGHT 64

int mapdata[MAP_DIM][MAP_DIM] = {
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    {1, 1, 1, 1, 1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 1, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 1},
    {1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 1},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
};

int textures_len = 0;
SDL_Surface **textures = NULL;

v2f pos   = {11.0f, 3.0f};
v2f dir   = {-1.0f, 0.0f};
v2f plane = {0.0f, 0.66f};

int rc_textured = 1;

#define R_360 (2.0f * M_PI)
#define rot_step (R_360 / 72.0f)

float mov_step = 0.5f;

void set_rgb_pixel(uint8_t *dst, const uint8_t *src) {
    dst[0] = src[0];
    dst[1] = src[1];
    dst[2] = src[2];
}

void rc_toggleTextures(void) {
    rc_textured = (rc_textured == 0) ? 1 : 0;
}

void rc_rotate(float r) {
    v2frot(&dir, r);
    v2frot(&plane, r);
}

void rc_rotateLeft(void) {
    rc_rotate(rot_step);
}

void rc_rotateRight(void) {
    rc_rotate(-rot_step);
}

void rc_moveForward(void) {
    float px, py;

    px = pos.x + mov_step * dir.x;
    py = pos.y + mov_step * dir.y;

    if (mapdata[(int)px][(int)pos.y] == 0) {
        pos.x = px;
    }
    if (mapdata[(int)pos.x][(int)py] == 0) {
        pos.y = py;
    }
    printf("rc_moveForward();  pos: [%.2f, %.2f];\n", pos.x, pos.y);
}

void rc_moveBackward(void) {
    const float px = pos.x - mov_step * dir.x;
    const float py = pos.y - mov_step * dir.y;

    if (mapdata[(int)px][(int)pos.y] == 0) {
        pos.x = px;
    }
    if (mapdata[(int)pos.x][(int)py] == 0) {
        pos.y = py;
    }

    printf("rc_moveBackward(); pos: [%.2f, %.2f];\n", pos.x, pos.y);
}
void rc_strafeLeft(void) {
    const float px = pos.x - mov_step * plane.x;
    const float py = pos.y - mov_step * plane.y;

    if (mapdata[(int)px][(int)pos.y] == 0) {
        pos.x = px;
    }
    if (mapdata[(int)pos.x][(int)py] == 0) {
        pos.y = py;
    }

    printf("rc_strafeLeft();   pos: [%.2f, %.2f];\n", pos.x, pos.y);
}

void rc_strafeRight(void) {
    float px, py;

    px = pos.x + mov_step * plane.x;
    py = pos.y + mov_step * plane.y;

    if (mapdata[(int)px][(int)pos.y] == 0) {
        pos.x = px;
    }
    if (mapdata[(int)pos.x][(int)py] == 0) {
        pos.y = py;
    }

    printf("rc_strafeRight();  pos: [%.2f, %.2f];\n", pos.x, pos.y);
}

void rc_initTextures(void) {
    const char *texture_names[] = {
        "../tex/tex1.bmp",
        "../tex/tex2.bmp",
        NULL
    };
    const char **ptr;
    SDL_Surface *tex;

    rc_freeTextures();

    textures_len = (sizeof(texture_names) / sizeof(texture_names[0])) - 1;
    textures = (SDL_Surface **)malloc(sizeof(SDL_Surface *) * textures_len);
    printf("%d textures allocated.\n", textures_len);

    for(ptr = texture_names; *ptr != NULL; ptr++) {
        if((textures[ptr - texture_names] = SDL_LoadBMP(*ptr)) == NULL) {
            printf("SDL_GetError() while loading '%s'. (msg: %s).", *ptr, SDL_GetError());
        }
        tex = *(textures + (ptr - texture_names));
        printf("Texture [i:%d, path:'%s', w:%d; h:%d; pitch:%d];\n", 
                ptr - texture_names, *ptr, tex->w, tex->h, tex->pitch);
    }
}

void rc_freeTextures(void) {
    if (textures != NULL) {
        for(int i = 0; i < textures_len; i++) {
            SDL_FreeSurface(textures[i]);
        }

        textures = NULL;
        textures_len = 0;
    }
}

void rc_main(void) {
    unsigned char quit = 0;
    SDL_Event event;

    while(quit == 0) {
        while(SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                quit = 1;
            } else if (event.type == SDL_KEYDOWN) {
                switch(event.key.keysym.sym) {
                case SDLK_ESCAPE:
                    quit = 1;
                    break;

                case SDLK_UP:
                case SDLK_w:
                    rc_moveForward();
                    break;

                case SDLK_DOWN:
                case SDLK_s:
                    rc_moveBackward();
                    break;

                case SDLK_LEFT:
                    rc_rotateLeft();
                    break;
                case SDLK_a:
                    rc_strafeLeft();
                    break;

                case SDLK_RIGHT:
                    rc_rotateRight();
                    break;
                case SDLK_d:
                    rc_strafeRight();
                    break;

                case SDLK_F1:
                    rc_toggleTextures();
                    break;

                default:
                    break;
                }
                window_update();
            }
        } /* end: while */
        SDL_Delay(10);
    } /* end: while */
}

void drawVerticalLine(image_t *img,
        ray_t *ray,
        span_t *img_span,
        int type) {
    uint8_t color[3];
    uint8_t *pos;
    int y_step;
    int y1, y2;

    switch(type) {
    case 1:
        color[0] = 80 ; color[1] = 80 ; color[2] = 80 ;
        break;
    case 2:
        color[0] = 48 ; color[1] = 48 ; color[2] = 48 ;
        break;
    case 3:
        color[0] = 128; color[1] = 128; color[2] = 128;
        break;
    default:
        color[0] = color[1] = color[2] = 255;
        break;
    }

    if (ray->side == SIDE_HORIZONTAL) {
        color[0] >>= 1;
        color[1] >>= 1;
        color[2] >>= 1;
    }

    y_step = 4 * img->width;

    pos = img->data + img_span->y1 * y_step + 4 * img_span->x;

    y1 = img_span->y1;
    y2 = img_span->y2;

    while(y1 < y2) {
        set_rgb_pixel(pos, color);
        pos += y_step;
        y1++;
    }
}

void textureWallSpan(image_t *img, 
        ray_t *ray,
        span_t *img_span,
        SDL_Surface *tex_surface,
        int tex_x) {
    float square_hstep;
    float square_hpos;
    uint8_t *img_ptr;
    uint8_t *tex;
    int img_step;
    int tex_pos;
    int y1, y2;
    int dark;
    int wh;
    int y;

    y1 = img_span->y1;
    y2 = img_span->y2;

    wh = y2 - y1;
    if (wh <= 0) {
        return;
    }

    /* square_hpos: 0 -> 64 */
    square_hstep = 64.0f / (float)wh;
    square_hpos  = y1 < 0 ? (float)(-y1) * square_hstep : 0.0f;

    if (y1 < 0) {
        y1 = 0;
    }
    if (y2 >= img->height) {
        y2 = img->height - 1;
    }

    tex = tex_surface->pixels;
    img_step = 4 * img->width;
    img_ptr  = img->data + img_step * y1 + 4 * img_span->x;
    dark = (ray->side == SIDE_VERTICAL) ? 0 : 1;

    tex_x = tex_x % TEX_WIDTH;
    for (y = y1; y < y2; y++) {
        tex_pos = 3 * (TEX_HEIGHT * ((int)square_hpos % TEX_HEIGHT) + tex_x);
        img_ptr[0] = tex[tex_pos + 0] >> dark;
        img_ptr[1] = tex[tex_pos + 1] >> dark;
        img_ptr[2] = tex[tex_pos + 2] >> dark;
        /* img_ptr[3] = 255; */

        img_ptr += img_step;
        square_hpos += square_hstep;
    }
}

void textureFlatSpan(image_t *img,
        ray_t *ray,
        span_t *img_span,
        SDL_Surface *floor_tex,
        SDL_Surface *ceil_tex,
        float square_pos) {
    uint8_t *img_ptr;
    float fx, fy;
    float y_fx, y_fy;
    int y_tx, y_ty;
    float yDist;
    float tDist;
    int ftex_pos, ctex_pos;
    int img_pos;
    int y;

    if (ray->side == SIDE_VERTICAL) {
        if (ray->dir.x > 0.0f) {
            fx = (float)ray->map.x;
            fy = (float)ray->map.y + square_pos;
        } else {
            fx = (float)ray->map.x + 1.0f;
            fy = (float)ray->map.y + square_pos;
        }
    } else {
        if (ray->dir.y > 0.0f) {
            fx = (float)ray->map.x + square_pos;
            fy = (float)ray->map.y;
        } else {
            fx = (float)ray->map.x + square_pos;
            fy = (float)ray->map.y + 1.0f;
        }
    }

    img_ptr = img->data;
    for (y = 0; y < img_span->y1; y++) {
        yDist = (float)img->height / ((float)img->height - 2.0f * (float)y);

        /* tDist \in [0, 1] 
         * (y=0, 0) -> (y=y1, 1) */
        tDist = yDist / ray->realDist;

        y_fx = (1.0f - tDist) * (ray->pos.x) + tDist * fx;
        y_fy = (1.0f - tDist) * (ray->pos.y) + tDist * fy;

        y_tx = ((int)(y_fx * (float)TEX_WIDTH)) % TEX_WIDTH;
        y_ty = ((int)(y_fy * (float)TEX_HEIGHT)) % TEX_HEIGHT;

        ftex_pos = 3 * (TEX_WIDTH * y_ty + y_tx);
        ctex_pos = 3 * (TEX_WIDTH * y_ty + y_tx);
        img_pos  = 4 * (img->width * y + img_span->x);

        set_rgb_pixel(img_ptr + img_pos, (uint8_t *)floor_tex->pixels + ftex_pos);

        img_pos = 4 * (img->width * (img->height - y - 1) + img_span->x);
        set_rgb_pixel(img_ptr + img_pos, (uint8_t *)ceil_tex->pixels + ctex_pos);
    }
}

void rc_draw(image_t *img) {
    v2f deltaDist;
    v2i step;

    span_t span;
    ray_t ray;

    float t;

    int swall_h;
    int x;

    printf("rc_draw(); pos=[%g, %g]; dir=[%g, %g];\n", pos.x, pos.y, dir.x, dir.y);

    ray.pos.x = pos.x;
    ray.pos.y = pos.y;

    for (x = 0; x < img->width; x++) {
        /* t \in [-1, 1] */
        t = -1.0f + ((float)x / (float)img->width) * 2.0f;

        /* ray_dir = dir + t * plane */
        ray.dir.x = dir.x + t * plane.x;
        ray.dir.y = dir.y + t * plane.y;

        ray.map.x = (int)ray.pos.x;
        ray.map.y = (int)ray.pos.y;

        ray.tg  = ray.dir.y / ray.dir.x;
        ray.ctg = 1.0f / ray.tg;

        /* deltaDist.x = sqrtf(1 + (ray_dir.y * ray_dir.y) / (ray_dir.x * ray_dir.x)); */
        deltaDist.x = sqrtf(1 + ray.tg * ray.tg);

        /* deltaDist.y = sqrtf(1 + (ray_dir.x * ray_dir.x) / (ray_dir.y * ray_dir.y)); */
        deltaDist.y = sqrtf(1 + ray.ctg * ray.ctg);

        if (ray.dir.x >= 0.0f) {
            step.x = 1;
            /* (ray_m.x + 1 - ray.x) \in [0, 1] */
            ray.dist.x = ((float)ray.map.x + 1.0f - ray.pos.x) * deltaDist.x;
        } else {
            step.x = -1;
            /* (ray.x - ray_m.x) \in [0, 1] */
            ray.dist.x = (ray.pos.x - (float)ray.map.x) * deltaDist.x;
        }

        if (ray.dir.y >= 0.0f) {
            step.y = 1;
            ray.dist.y = ((float)ray.map.y + 1.0f - ray.pos.y) * deltaDist.y;
        } else {
            step.y = -1;
            ray.dist.y = (ray.pos.y - (float)ray.map.y) * deltaDist.y;
        }

        ray.hit = 0;
        ray.side = 0;
        while(ray.hit == 0) {
            if (ray.dist.x < ray.dist.y) {
                ray.dist.x  += deltaDist.x;
                ray.map.x   += step.x;
                ray.side     = 0;
            } else {
                ray.dist.y  += deltaDist.y;
                ray.map.y   += step.y;
                ray.side     = 1;
            }

            if (mapdata[ray.map.x][ray.map.y] > 0) {
                ray.hit = 1;
            }
        }

        /*
         * x = ((float)(ray.map.x + ((1 - step.x) / 2)) - ray.pos.x);
         * y = ((float)(ray.map.y + ((1 - step.y) / 2)) - ray.pos.y);
         *
         * x / ray.dir.x == x / (|dir| == 1)
         * y / ray.dir.y == y / (|dir| == 1)
         *
         */
        if (ray.side == SIDE_VERTICAL) {
            /* (raydir.x < 0) -> dx = (ray_m.x - ray.x + 1) */
            ray.dx = ((float)(ray.map.x + ((1 - step.x) / 2)) - ray.pos.x);
            ray.realDist = fabs(ray.dx / ray.dir.x);
        } else {
            /* (raydir.x < 0) -> dx = (ray_m.x - ray.x + 1) */
            ray.dx = ((float)(ray.map.y + ((1 - step.y) / 2)) - ray.pos.y);
            ray.realDist = fabs(ray.dx / ray.dir.y);
        }

        swall_h = (int) ((float)img->height / ray.realDist);

        span.y1 = (img->height - swall_h) / 2;
        span.y2   = (img->height + swall_h) / 2;
        span.x = x;

        if (rc_textured) {
            float square_pos;
            SDL_Surface *tex;
            int tx;
            
            /* printf("[%3d] side: %d; rdir:(%g, %g); dx: %g; (rtg, rctg)=(%g, %g)\n", x, side, ray_dir.x, ray_dir.y, dx, rtg, rctg); */

            /* Dokładna pozycja trafionego miejsca: */
            if (ray.side == SIDE_VERTICAL) {
                square_pos = ray.pos.y + ray.dx * ray.tg;
            } else {
                square_pos = ray.pos.x + ray.dx * ray.ctg;
            }

            /* [0, 1] */
            square_pos = (square_pos - floorf(square_pos));

            tex = textures[(mapdata[ray.map.x][ray.map.y] - 1) % textures_len];
            tx  = (int)(64.0f * square_pos);

            textureWallSpan(img, &ray, &span, tex, tx);
            textureFlatSpan(img, &ray, &span, textures[0], textures[0], square_pos);
        } else {
            drawVerticalLine(img, &ray, &span, mapdata[ray.map.x][ray.map.y]);
        }
    }
}
